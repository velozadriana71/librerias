const customers = ['Adri', 'Adriana', 'Carmen'];

const activeCustomers = ['Adri', 'Carmen'];

const inactiveCustomers = _.difference(customers, activeCustomers);

console.log(inactiveCustomers);